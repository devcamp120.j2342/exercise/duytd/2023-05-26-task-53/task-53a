import models.Rectangle;
public class Task_53A20 {

    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.0f, 3.0f);
        System.out.println("rectangle 1");
        System.out.println(rectangle1.toString());
        System.out.println("Diện tích");
        System.out.println(rectangle1.getArea());
        System.out.println("Chu vi");
        System.out.println(rectangle1.getPerimeter());
        
        System.out.println("rectangle 2");
        System.out.println(rectangle2.toString());
        System.out.println("Diện tích");
        System.out.println(rectangle2.getArea());
        System.out.println("Chu vi");
        System.out.println(rectangle2.getPerimeter());
    }
}
